﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EvernoteOAuthNet;

namespace OAuthSample
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to start authorizing.");
            Console.ReadKey(true);


            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Authorizing...");
                Console.WriteLine();

                string myConsumerKey = "kamilgarey";
                string myConsumerSecret = "ad6891504327c013";
                EvernoteOAuth oauth = new EvernoteOAuth(EvernoteOAuth.HostService.Sandbox, myConsumerKey, myConsumerSecret, true);


                string errResponse = oauth.Authorize();
                if (errResponse.Length == 0)
                {
                    Console.WriteLine("Token: {0}\r\n\r\nExpires: {1}\r\n\r\nNoteStoreUrl: {2}\r\n\r\nUserId: {3}\r\n\r\nWebApiUrlPrefix: {4}\r\n\r\nLinked App Notebook was selected: {5}", oauth.Token, oauth.Expires, oauth.NoteStoreUrl, oauth.UserId, oauth.WebApiUrlPrefix, oauth.LinkedAppNotebookSelected.ToString());
                }
                else
                {
                    Console.WriteLine("A problem has occurred in attempting to authorize the use of your Evernote account: " + errResponse);
                }

                Console.WriteLine();
                Console.WriteLine("Press \"Enter\" to authorize another account.");

                if (Console.ReadKey(true).Key != ConsoleKey.Enter)
                {
                    break;
                }
            }

        }
    }
}
