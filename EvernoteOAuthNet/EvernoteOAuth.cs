﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace EvernoteOAuthNet
{
    public class EvernoteOAuth
    {

        #region Private Fields

        private readonly string _consumerKey;
        private readonly string _consumerSecret;
        private readonly HostService _service;
        private readonly string _windowTitle;

        private oAuthEvernote _oauth;

        #endregion


        #region Properties

        public string Token { get; set; }
        public string NoteStoreUrl { get; set; }
        public string UserId { get; set; }
        public long Expires { get; set; }
        public string WebApiUrlPrefix { get; set; }
        public bool SupportLinkedAppNotebook { get; set; } = false;
        public bool LinkedAppNotebookSelected { get; set; }

        #endregion


        public enum HostService { Production, Sandbox, Yinxiang };


        #region Constructors

        public EvernoteOAuth(HostService service, string consumerKey, string consumerSecret, string windowTitle = "Please provide authorization to access your Evernote account")
        {
            _service = service;
            _consumerKey = consumerKey;
            _consumerSecret = consumerSecret;
            _windowTitle = windowTitle;
            SupportLinkedAppNotebook = false;
        }

        public EvernoteOAuth(HostService service, string consumerKey, string consumerSecret, bool supportLinkedAppNotebook, string windowTitle = "Please provide authorization to access your Evernote account")
        {
            _service = service;
            _consumerKey = consumerKey;
            _consumerSecret = consumerSecret;
            SupportLinkedAppNotebook = supportLinkedAppNotebook;
            _windowTitle = windowTitle;
        }

        #endregion


        public string Authorize()
        {
            try
            {
                _oauth = new oAuthEvernote(_service, _consumerKey, _consumerSecret, SupportLinkedAppNotebook, _windowTitle);
                string requestToken = _oauth.getRequestToken();

                _oauth.authorizeToken(_windowTitle);


                NameValueCollection accessInfo = _oauth.getAccessToken();

                Token = accessInfo["oauth_token"];
                NoteStoreUrl = accessInfo["edam_noteStoreUrl"];
                UserId = accessInfo["edam_userId"];
                Expires = Convert.ToInt64(accessInfo["edam_expires"]);
                WebApiUrlPrefix = accessInfo["edam_webApiUrlPrefix"];
                LinkedAppNotebookSelected = _oauth.LinkedAppNotebookSelected;

                return string.Empty;

            }
            catch (Exception exp)
            {
                return exp.Message;
            }
        }

    }
}
