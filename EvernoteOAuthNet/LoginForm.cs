﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Collections.Specialized;
using System.Threading;
using System.Runtime.InteropServices;

namespace EvernoteOAuthNet
{
    internal partial class LoginForm : Form
    {

        #region Private Feilds

        private readonly oAuthEvernote _oauth;

        #endregion


        #region Properties

        public string Token { get; private set; }
        public string Verifier { get; private set; }
        public string TokenSecret { get; }
        public bool LinkedAppNotebookSelected { get; private set; }

        #endregion



        #region Constructors

        public LoginForm(oAuthEvernote o, string windowTitle)
        {
            _oauth = o;
            Token = null;
            InitializeComponent();
            this.Text = windowTitle;
            this.addressTextBox.Text = o.AuthorizationLink;
            Token = _oauth.Token;
            TokenSecret = _oauth.TokenSecret;


            browser.Navigated += AuthorizeOnceOnLogout;

            // Logout first from any signed account.
            browser.Navigate(new Uri(_oauth.LOGOUT_TOKEN));
        }

        private void AuthorizeOnceOnLogout(object sender, WebBrowserNavigatedEventArgs e)
        {
            try
            {
                // Authorize.
                browser.Navigate(new Uri(_oauth.AuthorizationLink));
            }
            finally
            {
                browser.Navigated -= AuthorizeOnceOnLogout;
            }
        }

        #endregion


        #region Form

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            browser.Dispose();
            CleanMemory();
        }

        #endregion


        #region Memory Cleanup

        [DllImport("kernel32.dll", EntryPoint = "SetProcessWorkingSetSize", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        private static extern int SetProcessWorkingSetSize(IntPtr process, int minimumWorkingSetSize, int maximumWorkingSetSize);

        private void CleanMemory()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                SetProcessWorkingSetSize(System.Diagnostics.Process.GetCurrentProcess().Handle, -1, -1);
            }
        }

        #endregion


        #region Browser

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            this.addressTextBox.Text = e.Url.ToString();
        }

        private void browser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (e.Url.ToString().Contains("log_out"))
            {
                browser.Stop();
                browser.Navigate(new Uri(_oauth.AuthorizationLink));
            }
            else if (!browser.Url.ToString().Contains(@"evernote.com") && !browser.Url.ToString().Contains(@"yinxiang.com"))
            {
                string queryParams = e.Url.Query;
                if (queryParams.Length > 0)
                {
                    //Store the Token and Token Secret
                    NameValueCollection qs = HttpUtility.ParseQueryString(queryParams);
                    if (qs["oauth_token"] != null)
                    {
                        Token = qs["oauth_token"];
                    }
                    if (qs["oauth_verifier"] != null)
                    {
                        Verifier = qs["oauth_verifier"];
                    }
                    if (qs["sandbox_lnb"] != null)
                    {
                        LinkedAppNotebookSelected = qs["sandbox_lnb"] == "true" ? true : false;
                    }
                }
                this.Close();
            }
        }

        private void addressTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Cancel the key press so the user can't enter a new url
            e.Handled = true;
        }

        #endregion

    }
}
